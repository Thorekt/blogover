CREATE TABLE `redacteur` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(30) NOT NULL,
  `email` varchar(36) NOT NULL,
  `passe` int(11) NOT NULL,
  PRIMARY KEY (`id`)
);

INSERT INTO `redacteur` (`id`, `nom`, `email`, `passe`) VALUES
(1, 'riri', 'riri@donaldville.fr', '123456'),
(2, 'fifi', 'fifi@donaldville.fr', '123456'),
(3, 'loulou', 'loulou@donaldville.fr', '123456');

CREATE TABLE `article` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `titre` varchar(30) NOT NULL,
  `ladate` date NOT NULL,
  `redacteur` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  FOREIGN KEY (`redacteur`) REFERENCES redacteur (`id`)
);