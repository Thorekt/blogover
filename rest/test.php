<?php
if (isset($_GET["id"]) || !empty($_GET["id"])){
    $id = $_GET["id"];
    $id -= 1;
    $jsonTab = json_decode(file_get_contents('articles\blogover.json'));
    $article = $jsonTab->{"articles"}[$id];
    echo json_encode($article);
}
else
    return readfile('articles\blogover.json');
?>